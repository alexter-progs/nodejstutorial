const expect = require('expect');
const rewire = require('rewire');

var app = rewire('./app');

describe('App', () => {
    var db = {
        saveUser: expect.createSpy()
    };

    app.__set__('db', db);

    it('should call the spy correctly', () => {
        var spy = expect.createSpy();
        spy('Alex', 22);
        expect(spy).toHaveBeenCalledWith('Alex', 22);
    });

    it('should call saveUser with user object', () => {
        let email = 'alexplotbox@gmail.com'
        let password = '123abc';

        app.handleSignup(email, password);
        expect(db.saveUser).toHaveBeenCalledWith({ email, password });
    });
})