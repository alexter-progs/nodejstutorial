const utils = require('./utils');
const expect = require('expect');

describe('Utils', () => {
    describe('#add', () => {
        it('should add two numbers', () => {
            let expectedValue = 44;
            let result = utils.add(33, 11);

            expect(result).toBe(expectedValue).toBeA('number');
        });
    });
    

    it('should square a number', () => {
        let expectedValue = 144;

        let result = utils.square(12);

        expect(result).toBe(expectedValue).toBeA('number');
    });

    it('should async add two numbers', (done) => {
        utils.asyncAdd(4, 3, (sum) => {
            expect(sum).toBe(7).toBeA('number');
            done();
        });
    });

    it('should async square two numbers', (done) => {
        utils.asyncSquare(5,5, (res) => {
            expect(res).toBe(25).toBeA('number');
            done();
        });

    });
});


it('should split fullName into first and last names', () => {
        let user = { };
        let expectedFirstName = 'Alexander'
        let expectedLastName = 'Plotnikov'

        user = utils.setName(user, `${expectedFirstName} ${expectedLastName}`);

        expect(user).toBeA('object').toInclude({
            firstName: expectedFirstName,
            lastName: expectedLastName
        })
});