const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.status(404).send({
        error: 'Page not found.',
        name: 'Todo App v1.0'
    });
});

app.get('/users', (req, res) => {
    users = [
        { name: 'Alex', age: 22 },
        { name: 'Slava', age: 22 },
        { name: 'Angelina', age: 23 }
    ]
    res.status(200).send(users);
})

app.listen(3001);

module.exports.app = app;