const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = 8080;

let app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));


app.use((req, res, next) => {
    let now = new Date().toString();
    let log = `${now}: ${req.method} ${req.url}`;

    console.log(log);
    fs.appendFile('server.log', log + '\n', (err) => {
        if(err) {
            console.log('Unable to append to server.log.');
        }
    });
    next();
});

//app.use((req, res, next) => {
//    res.render('maintanance');
//});

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase();
})

app.get('/', (req, res) => {
    res.render('home', {
        title: 'Home',
        welcomeMessage: 'Yey, welcom welcome'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About page'
    });
});

app.get('/projects', (req, res) => {
    res.render('projects', {
        title: 'Projects'
    })
});

app.listen(port, () => {
    console.log(`Server is up and runing on port ${port}`);
});