const request = require('request');
const yargs = require('yargs');
const weather = require('./weather/weather');

const geocode = require('./geocode/geocode');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.geocodeAddress(argv.address)
    .then(geo => {
        return weather.getWeather(geo.lattitude, geo.longitude);
    }, (error) => console.log(error))
    .then(results => {
        console.log(`Current temperature at ${argv.address} is ${results.temperature}, feels like ${results.apparentTemperature}`);
    }, (error) => console.log(error));



//bc849fc7a68a837075799ab351b2b7eb

