const request = require('request');
const geocode = require('../geocode/geocode');

var getWeather = (lattitude, longitude) => {
    return new Promise((resolve, reject) => {
        request({
            url: `https://api.darksky.net/forecast/bc849fc7a68a837075799ab351b2b7eb/${lattitude},${longitude}`,
            json: true
        }, (error, response, body) => {
            if(!error && response.statusCode === 200) {
                resolve({
                    temperature: body.currently.temperature,
                    apparentTemperature: body.currently.apparentTemperature
                });
            } else {
                reject('Unable to fetch weather.');
            }
        });  
    })
};

module.exports.getWeather = getWeather;
