'use strict'

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes');

const options = {
    title: {
        describe: 'Title of note',
        demand: true,
        alias: 't'
    },
    body: {
        describe: 'Body of note',
        demand: true,
        alias: 'b'
    }
}

const argv = yargs
    .command('add', 'Add a new note', {
        title: options.title,
        body: options.body
    })
    .command('read','Read a note', {
        title: options.title
    })
    .command('remove', 'Remove a note', {
        title: options.title
    })
    .command('list', 'List all notes')
    .help()
    .argv;
let command = argv._[0];

switch(command) {
    case 'add': {
        let note = notes.addNote(argv.title, argv.body);
        if(note) {
            console.log('Note created');
            notes.logNote(note);
        } else {
            console.log('Note title taken');
        }
        break;
    }
    case 'read': {
        let note = notes.getNote(argv.title);
        if(note) {
            console.log('Note found');
            notes.logNote(note);
        } else {
            console.log('Note not found');
        }
        break;
    }
    case 'list': {
        let allNotes = notes.getAll();
        console.log(`Listing ${allNotes.length} note(s).`);
        allNotes.forEach(note => notes.logNote(note));
        break;
    }
    case 'remove': {
        let removed = notes.removeNote(argv.title);
        let message = removed === true ? 'Note removed' : 'Note not found';
        console.log(message);
        break;
    }
    default: {
        console.log('Command not recognized');
    }
}
