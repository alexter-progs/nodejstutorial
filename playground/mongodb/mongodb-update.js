const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID("5970c0602b94ed48a4dc0128")
    }, {
        $set: {
            completed: false
        }
    }, {
        returnOriginal: false
    }).then(res => {
        console.log(res);
    });

});